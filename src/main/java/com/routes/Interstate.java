package com.routes;

public class Interstate extends Routes {

	
	private int routeNumber;
	private String roadNoCategory;
	private Orientations orientation;
	private int I;
	private String location;


	@Override
	public String getHighwayDirection(String highwayName) {
	int highwayNo = Integer.parseInt(highwayName.substring(1));
	System.out.println( highwayNo);
	
	return getInterStateLocation(highwayNo);
		
		//return highwayNo;
	}
	
	public void getOrientationDirection() {
		
		if(roadNoCategory.equals("EvenNoRoads")) {
			orientation =Orientations.eastWest;
		}
		else if(roadNoCategory.equals("OddNoRoads")){
			orientation = Orientations.northSouth;
		}
		else {
			System.out.println("No orientation");
		}
		
	}

	public String getInterStateLocation(int I) {
		if(I >=1 && I <=99) {
			
			
			if(I%2 !=0 && I<50) {
				
				
				orientation = Orientations.northSouth;
				location = "west";
			}
			else if(I%2 !=0 && I >=51) {
				
				orientation = Orientations.northSouth;
				location = "east";
			}
			else if(I%2 == 0 && I<49){
				
				orientation = Orientations.eastWest;
				location = "south";
			}
			else if(I%2 == 0 && I>=50) {
				
				orientation = Orientations.eastWest;
				location = "north";
			}
		}
		else {
			location = "Unknown location";
		}
		return location;
		
	}
	
	public int getRouteNumber() {
		return routeNumber;
	}

	public void setRouteNumber(int routeNumber) {
		this.routeNumber = routeNumber;
	}

	public String getRoadNoCategory() {
		return roadNoCategory;
	}

	public void setRoadNoCategory(String roadNoCategory) {
		this.roadNoCategory = roadNoCategory;
	}

	public Orientations getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientations orientation) {
		this.orientation = orientation;
	}

	public int getI() {
		return I;
	}

	public void setI(int i) {
		I = i;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}



	
	
	
}
