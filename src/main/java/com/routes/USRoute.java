package com.routes;

public class USRoute extends Routes {

	private int routeNo;
	private Orientations orientation;
	private String location;

	@Override
	public String getHighwayDirection(String highwayName) {

		routeNo = Integer.parseInt(highwayName.substring(2));
		System.out.println(routeNo);

		return getUSRouteLocation(routeNo);
	}

	private String getUSRouteLocation(int routeNo) {

		if (routeNo >= 1 && routeNo <= 99) {

			if (routeNo % 2 != 0 && routeNo < 50) {

				orientation = Orientations.northSouth;
				location = "east";
			} else if (routeNo % 2 != 0 && routeNo >= 51) {

				orientation = Orientations.northSouth;
				location = "west";
			} else if (routeNo % 2 == 0 && routeNo < 49) {

				orientation = Orientations.eastWest;
				location = "north";
			} else if (routeNo % 2 == 0 && routeNo >= 50) {

				orientation = Orientations.eastWest;
				location = "south";
			}
		} else {
			location = "Unknown location";
		}
		return location;
	}

}
