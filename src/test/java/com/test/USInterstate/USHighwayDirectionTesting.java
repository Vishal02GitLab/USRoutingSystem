package com.test.USInterstate;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.routes.Interstate;

public class USHighwayDirectionTesting {

	Interstate state;
	@Before
	public void doSetUp() {
		state = new Interstate();
	}
	
	@Test
	public void testHighwayDirectionOddWesternHalf() {
		String expected = "west";
		assertEquals(expected,state.getHighwayDirection("I5"));
	}
	
	@Test
	public void testHighwayDirectionOddEasternHalf() {
		String expected = "east";
		assertEquals(expected,state.getHighwayDirection("I99"));
		
	}
	
	@Test
	public void testHighwayDirectionEvenNorthernHalf() {
		String expected = "north";
		assertEquals(expected,state.getHighwayDirection("I96"));
		
	}
	
	@Test
	public void testHighwayDirectionEvenSouthernHalf() {
		String expected = "south";
		assertEquals(expected,state.getHighwayDirection("I48"));
		
	}

}
