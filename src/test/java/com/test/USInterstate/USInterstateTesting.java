package com.test.USInterstate;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.routes.Interstate;

public class USInterstateTesting {

	Interstate state;

	@Before
	public void doSetUp() {
		state = new Interstate();
	}

	/*
	 * @Test public void testInterstateOrientations() {
	 * state.setRoadNoCategory("EvenNoRoads"); String expected = "East-West"; String
	 * actual = state.getOrientationDirection();
	 * 
	 * System.out.println(actual); assertEquals(expected,actual); }
	 */

	@Test
	public void testUSInterstateLocation() {
		state.setI(45);
		String expected = "west";
		String actual = state.getInterStateLocation(45);
		System.out.println("testUSInterstateLocation: " + actual);
		assertEquals(expected, actual);

	}

	@Test
	public void testUSInterstateLocationForEvenValue() {
		state.setI(22);
		String expected = "south";
		String actual = state.getInterStateLocation(22);
		System.out.println("testUSInterstateLocationForEvenValue: " + actual);
		assertEquals(expected, actual);
	}

	@Test
	public void testUSInterstateLocationForEvenValueMaxRouteNo() {
		state.setI(98);
		String expected = "north";
		String actual = state.getInterStateLocation(98);
		System.out.println("testUSInterstateLocationForEvenValueMaxRouteNo: " + actual);
		assertEquals(expected, actual);
	}

}
