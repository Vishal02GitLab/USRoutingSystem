package com.test.USInterstate;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.routes.USRoute;

public class USRouteTesting {

	USRoute route;

	@Before
	public void doSetUp() {
		route = new USRoute();
	}

	@Test
	public void testUSRouteLowerNorthSouth() {

		String expected = "east";

		assertEquals(expected, route.getHighwayDirection("US17"));
	}

	@Test
	public void testUSRouteHigherNorthSouth() {
		String expected = "west";

		assertEquals(expected, route.getHighwayDirection("US99"));

	}

	@Test
	public void testUSRouteLowerEastWest() {

		String expected = "north";

		assertEquals(expected, route.getHighwayDirection("US46"));
	}

	@Test
	public void testUSRouteHigherEastWest() {

		String expected = "south";

		assertEquals(expected, route.getHighwayDirection("US96"));
	}

}
